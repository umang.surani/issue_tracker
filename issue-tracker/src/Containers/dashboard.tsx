import '../App.css';
import Card from '../components/card';

function Dashboard(prop: any) {
    console.log(prop);
    let count = prop.DoList.length;
    let ToDoCard = [];
    let InProgress = [];
    let Done = [];
    for (let index = 0; index < count; index++) {
        const model = prop.DoList[index];
        if (model.status === 'In Progress')  {
            InProgress.push(<Card clickEvent={prop.clickEvent} ToDoModel={model} key={index}/>)
        } else if(model.status === 'Complete') {
            Done.push(<Card clickEvent={prop.clickEvent} ToDoModel={model} key={index}/>);
        } else { 
            ToDoCard.push(<Card clickEvent={prop.clickEvent} ToDoModel={model} key={index}/>) 
        }
    }
    return (
        <div className="App App-header">
            <div>
                <h3>To Do {ToDoCard.length}</h3>
                {ToDoCard}
            </div>
            <div>
                <h3>In Progress {InProgress.length}</h3>
                {InProgress}
            </div>
            <div>
                <h3>Done {Done.length}</h3>
                {Done}
            </div>
        </div>
    )
}
export default Dashboard; 