import '../App.css';
import FormComponent from '../components/formcomponent';

function FormContainer(prop: any) { 
  return (
    <div>
      <FormComponent parentProp = {prop}  />
    </div>
  )
}
export default FormContainer; 