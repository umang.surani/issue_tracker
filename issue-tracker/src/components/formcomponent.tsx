import "./form.css";

const Assignee = ['Test Suraname', 'Test Suraname1', 'Test Suraname2', 'Test Suraname3', 'Test Suraname4'];
const Reporter = ['Test Reporter', 'Test Reporter1', 'Test Reporter2', 'Test Reporter3', 'Test Reporter4'];
const StoryPonits = ['1', '3', '5', '8'];
const Status = ['In Progress', 'ToDo', 'Complete'];
const Priority = ['Low', 'High'];

function FormComponent(prop: any) {
  return (
    <form onSubmit={prop.parentProp.onSubmit}>
      <h3>Edit Issue Page</h3>
      <div className="from-contanier">
        <span>
          <label>Componets/s:</label>
          <input value={prop.parentProp.values.componetsname} onChange={prop.parentProp.onName}
            type="text" required />
        </span>

        <span>
          <label>Assignee:</label>
          <select required value={prop.parentProp.values.assignee} onChange={prop.parentProp.onAssignee}>
            <option value="">Select assignee</option>
            {Assignee.map(c => <option key={c}>{c}</option>)}
          </select>
        </span>

        <span>
          <label>Labels:</label>
          <input value={prop.parentProp.values.labels} onChange={prop.parentProp.onLables}
            type="text" required />
        </span>

        <span>
          <label>Reporter:</label>
          <select required value={prop.parentProp.values.reporter} onChange={prop.parentProp.onReporter}>
            <option value="">Select Reporter</option>
            {Reporter.map(c => <option key={c}>{c}</option>)}
          </select>
        </span>

        <span>
          <label>Sprint:</label>
          <input value={prop.parentProp.values.sprint} onChange={prop.parentProp.onSprint}
            type="text" required />
        </span>

        <span>
          <label>Description:</label>
          <input value={prop.parentProp.values.description} onChange={prop.parentProp.onDescription}
            type="text" required />
        </span>

        <span>
          <label>Story Ponits:</label>
          <select value={prop.parentProp.values.storyponits} onChange={prop.parentProp.onStoryPonits}
            required>
            <option value="">Select Story Ponits</option>
            {StoryPonits.map(c => <option key={c}>{c}</option>)}
          </select>
        </span>

        <span>
          <label>Attachment:</label>
          <input />
        </span>

        <span>
          <label>Status:</label>
          <select value={prop.parentProp.values.status} onChange={prop.parentProp.onStatus}
            required>
            <option value="">Select Status</option>
            {Status.map(c => <option key={c}>{c}</option>)}
          </select>
        </span>

        <span>
          <label>Priority:</label>
          <select value={prop.parentProp.values.priority} onChange={prop.parentProp.onPrirority}
            required>
            <option value="">Select Priority</option>
            {Priority.map(c => <option key={c}>{c}</option>)}
          </select>
        </span>

      </div>
      <button type="submit">Submit</button>
    </form>
  );
}

export default FormComponent