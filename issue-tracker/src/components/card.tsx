import '../App.css';
import logo from '../logo.svg';


function Card(prop : any) { 
    const domodel = prop.ToDoModel; 
    return (
        <div className="card" onClick={()=> prop.clickEvent(domodel) }>
            <div className="container">
                <div className="id">
                    <span className="">ID:{domodel.id}</span>
                    <span>January 01 2021</span>
                </div>
                <div className="title">
                    <span><b>{domodel.componetsname}</b></span>
                </div>
                <div className="content">
                    <span>{domodel.description}</span>
                </div>
                <div className="footer">
                   <span>Assignee</span>
                   <span>Status</span> 
                </div>
                <div className="footer">
                    <div className="subfooter">
                        <img src={logo} className="App-logo" alt="logo" />
                        <span>
                            <span><b>{domodel.assignee}</b></span>
                            <span>{domodel.labels}</span>
                        </span>
                    </div>
                    <a className="card-tag">{domodel.priority}</a>
                </div>
            </div>
        </div>
    )
}

export default Card; 