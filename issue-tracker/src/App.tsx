import React, { ChangeEvent, useState } from 'react';
import './App.css';
import Dashboard from './Containers/dashboard'
import FormContainer from './Containers/formcontainer'

class IssueModel {
  id: number = 0;
  componetsname: string = '';
  assignee: string = '';
  labels: string = '';
  reporter: string = '';
  sprint: string = '';
  storyponits: string = '';
  status: string = '';
  description: string = '';
  priority: string = '';
}


function App() {  
  let DoList: IssueModel[] = [];
  for (let index = 0; index < 5; index++) { 
    let issueModel = new IssueModel();
    issueModel.componetsname = "Jone Doe";
    issueModel.id = 12312312312 + index;
    issueModel.description = "One advanced diverted domestic sex repeated bringing you old." +
      "Possible procured her trifling laughter thoughts property she met way." +
      "Companions shy had solicitude favourable own.";
    issueModel.priority = "High";
    issueModel.assignee = "Test Suraname";
    issueModel.status = "ToDo";
    issueModel.storyponits = "8";
    issueModel.labels = "UI/UX Designer";
    issueModel.reporter = "Test Reporter1";
    issueModel.sprint = "first";
    if (index % 2 === 0) {
      issueModel.priority = "Low";
      issueModel.status = "Complete";
    }
    if(index % 5 === 0) {
      issueModel.status = "In Progress";
    }
    DoList.push(issueModel);
  }
  

  const [values, setValues] = useState({
    componetsname: '', assignee: '', labels: '', reporter: '',
    sprint: '', description: '', storyponits: '', status: '',
    priority: '', id: 0
  });

  const [issueList, setIssueList] = useState(DoList);

  const setEditValuesInState = (value: string, statePropName: string) => {
    if (statePropName === 'id') {
      const id: number = +value
      setValues(oldValues => ({ ...oldValues, [statePropName]: id }));
    } else {
      setValues(oldValues => ({ ...oldValues, [statePropName]: value }));
    }
  }

  const set = (statePropName: string) => {
    return (event: ChangeEvent<HTMLInputElement>) => {
      setValues(oldValues => ({ ...oldValues, [statePropName]: event.target.value }));
      if (statePropName === 'reporter') {
        setEditValuesInState('', 'assignee');
      }
    }
  };

  const clickEvent = (issdueModel: IssueModel) => {
    setEditValuesInState(issdueModel.componetsname, 'componetsname');
    setEditValuesInState(issdueModel.assignee, 'assignee');
    setEditValuesInState(issdueModel.description, 'description');
    setEditValuesInState(issdueModel.labels, 'labels');
    setEditValuesInState(issdueModel.priority, 'priority');
    setEditValuesInState(issdueModel.reporter, 'reporter');
    setEditValuesInState(issdueModel.sprint, 'sprint');
    setEditValuesInState(issdueModel.status, 'status');
    setEditValuesInState(issdueModel.storyponits, 'storyponits');
    setEditValuesInState(issdueModel.id.toString(), 'id');
  }

  const onSubmit = (event: any) => {
    event.preventDefault(); // Prevent default submission
    try {
      saveFormData();
      setValues({
        componetsname: '', assignee: '', labels: '', reporter: '',
        sprint: '', description: '', storyponits: '', status: '',
        priority: '', id: 0
      });
    } catch (e) {
      alert(`Registration failed! ${e.message}`);
    }
  }

  const saveFormData = () => {
    if (values.id !== 0) {
      let updatelist: IssueModel[] = [];
      issueList.forEach(val => updatelist.push(Object.assign({}, val)));
      const editIndex = updatelist.findIndex(element => element.id === values.id);
      updatelist[editIndex].componetsname = values.componetsname;
      updatelist[editIndex].assignee = values.assignee;
      updatelist[editIndex].description = values.description;
      updatelist[editIndex].id = values.id;
      updatelist[editIndex].labels = values.labels;
      updatelist[editIndex].priority = values.priority;
      updatelist[editIndex].reporter = values.reporter;
      updatelist[editIndex].sprint = values.sprint;
      updatelist[editIndex].status = values.status;
      updatelist[editIndex].storyponits = values.storyponits;
      setIssueList(updatelist);
    }
  }

  return (
    <div>
      <Dashboard clickEvent={clickEvent} DoList={issueList} />
      <FormContainer values={values} onName={set('componetsname')} onAssignee={set('assignee')}
        onDescription={set('description')} onLables={set('labels')}
        onPrirority={set('priority')} onReporter={set('reporter')}
        onSprint={set('sprint')} onStatus={set('status')}
        onStoryPonits={set('storyponits')} onSubmit={onSubmit} />
    </div>
  );
}

export default App;
